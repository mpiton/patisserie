import { Document } from "mongoose";

export interface ICommande extends Document {
	produits: [
		{
			productId: string;
			quantity: number;
		}
	];
	montantTotal: number;
	pickupDate: Date;
	//TODO: StripeSessionId: string,
	status: string;
	remarque: string;
}
