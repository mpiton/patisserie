import { Document, Schema } from "mongoose";

export interface IPanier extends Document {
	productId: Schema.Types.ObjectId;
	quantity: number;
}
