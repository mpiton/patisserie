import { Decimal128, Document } from "mongoose";

export interface IProduit extends Document {
	nom: string;
	isBest: boolean;
	slug: string;
	categories: ICategoriesArray;
	image: {
		fieldname: string;
		originalname: string;
		encoding: string;
		mimetype: string;
		destination: string;
		filename: string;
		path: string;
		size: number;
	};
	prix: Decimal128;
	description: string;
}

interface ICategoriesArray {
	[index: number]: string;
}
