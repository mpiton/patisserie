import { Document, Schema } from "mongoose";

export interface IResetPassword extends Document {
	userId: Schema.Types.ObjectId;
	token: string;
}
