import { Document, Schema } from "mongoose";

export interface IHeader extends Document {
	productId: Schema.Types.ObjectId
}
