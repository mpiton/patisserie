import { Document } from "mongoose";

export interface ICategorie extends Document {
	nom: string;
	slug: string;
}
