import { Document, Schema } from "mongoose";

export interface ICommentaire extends Document {
	titre: string;
	createdBy: string;
	commentaire: string;
	note: number;
	productId: Schema.Types.ObjectId;
}
