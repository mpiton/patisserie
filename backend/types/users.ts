/* eslint-disable @typescript-eslint/no-explicit-any */
import { Document } from "mongoose";
import { ICommande } from "./commande";
import { IPanier } from "./panier";

export interface IUser extends Document {
	comparePasswords(
		password: string,
		arg1: (error: Error, isMatch: boolean) => Promise<void> | undefined
	): any;
	_id: string;
	nom: string;
	prenom: string;
	email: string;
	role: string[];
	salt: string;
	hashed_password: string;
	createdAt: Date;
	commandes: ICommande[];
	panier: IPanier[];
}
