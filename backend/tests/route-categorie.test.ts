import chai from "chai";
import chaiHttp from "chai-http";
import { connect, disconnect } from "../config/db";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

let token: string;
// Test CRUD Catégories
describe("CRUD Catégories", () => {
	before(async function () {
		await connect();
	});
	//Test GET api/v1/categories
	describe("GET ALL Catégories", () => {
		it("doit récupérer toutes les catégories si je suis Admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get("/api/v1/categories")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.categories.should.be.an("array");
							res.body.categories[0].should.have.property("nom");
							res.body.categories[0].should.have.property("slug");
							done();
						});
				});
		});
		it("ne doit pas récupérer les catégories si je ne suis pas admin", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get("/api/v1/categories")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	// GET /api/v1/categorie/:id
	describe("/GET/:id Catégorie", () => {
		it("doit récupérer une catégorie par son ID", (done) => {
			const sampleUser = {
				_id: "606aed0f55a7572b9c925f21",
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get("/api/v1/categorie/606c14b57b1626120813ddf0")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							done();
						});
				});
		});
		it("ne doit rien récupérer si l'ID est invalide", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get(`/api/v1/categorie/123456`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test DELETE api/v1/categorie/:id
	describe("DELETE Catégorie par son ID", () => {
		it("doit pouvoir supprimer une catégorie si je suis Admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/categorie/606c0fc4ab46ea3300fb8be7")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("categorie");
							done();
						});
				});
		});
		it("ne doit pas supprimer une catégorie si je ne suis pas admin", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/categorie/606c0fc4ab46ea3300fb8be7")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test PUT api/v1/categorie/:id
	describe("PUT Catégorie par son ID", () => {
		it("doit pouvoir update une catégorie si je suis admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.put(`/api/v1/categorie/606c14b57b1626120813ddf0`)
						.send({
							nom: "gâteau à la crème",
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("categorie");
							done();
						});
				});
		});
		it("ne doit pas update une catégorie si je ne suis pas admin", (done) => {
			chai
				.request(app)
				.put("/api/v1/categorie/606c14b57b1626120813ddf0")
				.send({
					nom: "gâteau aux fraises",
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});
	//Test ADD api/v1/categorie
	describe("ADD Catégorie", () => {
		it("doit pouvoir ajouter une catégorie si je suis admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.post(`/api/v1/categorie`)
						.send({
							nom: "la categorie",
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(201);
							res.body.should.be.a("object");
							res.body.should.have.property("nom");
							res.body.should.have.property("slug");
							done();
						});
				});
		});
		it("ne doit pas ajouter de catégorie si je ne suis pas admin", (done) => {
			chai
				.request(app)
				.post("/api/v1/categorie")
				.send({
					nom: "test",
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});

	after(async function () {
		disconnect();
	});
});
