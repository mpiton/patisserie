import chai from "chai";
import chaiHttp from "chai-http";
import { connect, disconnect } from "../config/db";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

let token: string;
// Test CRUD Headers
describe("CRUD Headers", () => {
	before(async function () {
		await connect();
	});
	//Test GET api/v1/headers
	describe("GET ALL Headers", () => {
		it("doit récupérer toutes les headers", (done) => {
			chai
				.request(app)
				.get("/api/v1/headers")
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(200);
					res.body.should.be.a("object");
					res.body.headers.should.be.an("array");
					res.body.headers[0].should.have.property("productId");
					done();
				});
		});
	});
	// GET /api/v1/header/:id
	describe("/GET/:id Header", () => {
		it("doit récupérer un header par son ID", (done) => {
			chai
				.request(app)
				.get("/api/v1/header/606f2727ba8934493028ea05")
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(200);
					res.body.should.be.a("object");
					done();
				});
		});
		it("ne doit rien récupérer si l'ID est invalide", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get(`/api/v1/header/123456`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test DELETE api/v1/header/:id
	describe("DELETE Header par son ID", () => {
		it("doit pouvoir supprimer une header si je suis Admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/header/606f2727ba8934493028ea05")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("header");
							done();
						});
				});
		});
		it("ne doit pas supprimer une header si je ne suis pas admin", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/header/606f2727ba8934493028ea05")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test PUT api/v1/header/:id
	describe("PUT Header par son ID", () => {
		it("doit pouvoir update une header si je suis admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.put(`/api/v1/header/606f2727ba8934493028ea05`)
						.send({
							productId: "606f292e3d39d907e429932e",
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("header");
							done();
						});
				});
		});
		it("ne doit pas update une header si je ne suis pas admin", (done) => {
			chai
				.request(app)
				.put("/api/v1/header/606f2727ba8934493028ea05")
				.send({
					productId: "606f292e3d39d907e429932e",
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});
	//Test ADD api/v1/header
	describe("ADD Header", () => {
		it("doit pouvoir ajouter une header si je suis admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.post(`/api/v1/header`)
						.send({
							productId: "606f292e3d39d907e429932e",
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(201);
							res.body.should.be.a("object");
							res.body.should.have.property("productId");
							done();
						});
				});
		});
		it("ne doit pas ajouter de header si je ne suis pas admin", (done) => {
			chai
				.request(app)
				.post("/api/v1/header")
				.send({
					productId: "606f292e3d39d907e429932e",
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});

	after(async function () {
		disconnect();
	});
});
