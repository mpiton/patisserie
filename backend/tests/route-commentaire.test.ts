import chai from "chai";
import chaiHttp from "chai-http";
import { connect, disconnect } from "../config/db";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

let token: string;
// Test CRUD Commentaires
describe("CRUD Commentaires", () => {
	before(async function () {
		await connect();
	});
	//Test GET api/v1/commentaires
	describe("GET ALL Commentaires", () => {
		it("doit récupérer tous les commentaires", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get("/api/v1/commentaires")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.commentaires.should.be.an("array");
							res.body.commentaires[0].should.have.property("titre");
							res.body.commentaires[0].should.have.property("createdBy");
							res.body.commentaires[0].should.have.property("commentaire");
							res.body.commentaires[0].should.have.property("note");
							done();
						});
				});
		});
	});
	// GET /api/v1/commentaire/:id
	describe("/GET/:id Commentaire", () => {
		it("doit récupérer un commentaire par son ID", (done) => {
			chai
				.request(app)
				.get("/api/v1/commentaire/606f06c448e3fe12e83f0916")
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(200);
					res.body.should.be.a("object");
					res.body.commentaire.should.have.property("titre");
					res.body.commentaire.should.have.property("createdBy");
					res.body.commentaire.should.have.property("commentaire");
					res.body.commentaire.should.have.property("note");
					done();
				});
		});
		it("ne doit rien récupérer si l'ID est invalide", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get(`/api/v1/commentaire/123456`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test DELETE api/v1/commentaire/:id
	describe("DELETE Commentaire par son ID", () => {
		it("doit pouvoir supprimer un commentaire si je suis Admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/commentaire/606effe1f9cb5e47443b4df1")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("commentaire");
							done();
						});
				});
		});
		it("ne doit pas supprimer une catégorie si je ne suis pas admin", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/commentaire/606effe1f9cb5e47443b4df1")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test PUT api/v1/commentaire/:id
	describe("PUT Commentaire par son ID", () => {
		it("doit pouvoir update un commentaire si je suis admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.put(`/api/v1/commentaire/606effe1f9cb5e47443b4df1`)
						.send({
							titre: "titre modifié",
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("commentaire");
							done();
						});
				});
		});
		it("ne doit pas update une catégorie si je ne suis pas admin", (done) => {
			chai
				.request(app)
				.put("/api/v1/commentaire/606da7538cef170380e49bb3")
				.send({
					titre: "titre mis à jour",
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});
	//Test ADD api/v1/commentaire
	describe("ADD Commentaire", () => {
		it("doit pouvoir ajouter un commentaire à un produit si je suis connecté", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.post(`/api/v1/commentaire/606eeeaf58060748544c0150`)
						.send({
							titre: "le titre ici",
							commentaire: "commentaires ici",
							note: 3,
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(201);
							res.body.should.be.a("object");
							res.body.should.have.property("titre");
							res.body.should.have.property("commentaire");
							res.body.should.have.property("createdBy");
							res.body.should.have.property("note");
							done();
						});
				});
		});
		it("ne doit pas ajouter de commentaire si je ne suis pas connecté", (done) => {
			chai
				.request(app)
				.post("/api/v1/commentaire/606eeeaf58060748544c0150")
				.send({
					titre: "titre du commentaire",
					commentaire: "Commentaire du premier test",
					note: 3,
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});

	after(async function () {
		disconnect();
	});
});
