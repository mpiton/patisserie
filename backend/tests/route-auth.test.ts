import chai from "chai";
import chaiHttp from "chai-http";
import { after } from "mocha";
import { connect, disconnect } from "../config/db";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

let token: string;

// Test Login
describe("Login Tests", () => {
	before(async function () {
		await connect();
	});
	//Test POST api/v1/login
	describe("Login Utilisateur", () => {
		it("doit connecter un utilisateur", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(202);
					res.body.should.be.a("object");
					res.body.should.have.property("user");
					res.body.user.should.have.property("email").eql("mathieu@piton2.fr");
					done();
				});
		});
		it("ne doit pas connecter un utilisateur si le mot de passe est incorrect", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "qwerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have
						.property("error")
						.eql("Email et mot de passe incorrects !");
					done();
				});
		});
	});

	//Test POST api/v1/logout
	describe("Déconnexion Utilisateur", () => {
		it("doit déconnecter un utilisateur", (done) => {
			const sampleUser = {
				_id: "606aed0f55a7572b9c925f21",
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.post(`/api/v1/logout`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
		it("doit renvoyer une erreur si l'utilisateur n'est pas connecté avant la déconnexion", (done) => {
			chai
				.request(app)
				.post(`/api/v1/logout`)
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});

	after(async function () {
		disconnect();
	});
});
