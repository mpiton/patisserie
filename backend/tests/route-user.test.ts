import chai from "chai";
import chaiHttp from "chai-http";
import { connect, disconnect } from "../config/db";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

let token: string;
// Test CRUD Utilisateurs
describe("CRUD Utilisateurs", () => {
	before(async function () {
		await connect();
	});
	//Test GET api/v1/users
	describe("GET ALL Utilisateurs", () => {
		it("doit récupérer tous les utilisateurs si je suis Admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get("/api/v1/users")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.users.should.be.an("array");
							res.body.users[0].should.have.property("nom");
							res.body.users[0].should.have.property("prenom");
							res.body.users[0].should.have.property("email");
							res.body.users[0].should.have.property("role");
							done();
						});
				});
		});
		it("ne doit pas récupérer les utilisateurs si je ne suis pas admin", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get("/api/v1/users")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	// GET BY ID /api/v1/user/:id
	describe("/GET/:id Utilisateur", () => {
		it("doit récupérer un utilisateur par son ID", (done) => {
			const sampleUser = {
				_id: "606aed0f55a7572b9c925f21",
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get(`/api/v1/user/${sampleUser._id}`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							done();
						});
				});
		});
		it("ne doit rien récupérer si l'ID est invalide", (done) => {
			const sampleUser = {
				_id: "606aed0f55a7572b9c925f21",
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get(`/api/v1/user/123456`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
		//Test DELETE api/v1/user/:id
		describe("DELETE Utilisateur par son ID", () => {
			it("doit pouvoir supprimer un utilisateur si je suis Admin", (done) => {
				const sampleUser = {
					email: "mathieu@piton2.fr",
					password: "azerty",
				};
				chai
					.request(app)
					.post("/api/v1/login")
					.send(sampleUser)
					.then((res) => {
						token = res.body.token;
						chai
							.request(app)
							.delete("/api/v1/user/606adf8b11946a0d74af0c23")
							.set("Cookie", `t=${token}`)
							.end((err, res) => {
								if (err) {
									throw err;
								}
								res.should.have.status(200);
								res.body.should.be.a("object");
								res.body.should.have.property("message");
								res.body.should.have.property("user");
								res.body.should.have.property("users");
								res.body.users.should.be.a("array");
								done();
							});
					});
			});
			it("ne doit pas supprimer un utilisateur si je ne suis pas admin", (done) => {
				const sampleUser = {
					email: "test@gmail.fr",
					password: "azerty123",
				};
				chai
					.request(app)
					.post("/api/v1/login")
					.send(sampleUser)
					.then((res) => {
						token = res.body.token;
						chai
							.request(app)
							.delete("/api/v1/user/606adf0b05866c41e8608504")
							.set("Cookie", `t=${token}`)
							.end((err, res) => {
								if (err) {
									throw err;
								}
								res.should.have.status(401);
								res.body.should.be.a("object");
								res.body.should.have.property("message");
								done();
							});
					});
			});
		});
		//Test PUT api/v1/user/:id
		describe("PUT Utilisateur par son ID", () => {
			it("doit pouvoir update un utilisateur si la personne est connecté", (done) => {
				const sampleUser = {
					_id: "606aed0f55a7572b9c925f21",
					email: "test@gmail.fr",
					password: "azerty123",
				};
				chai
					.request(app)
					.post("/api/v1/login")
					.send(sampleUser)
					.then((res) => {
						token = res.body.token;
						chai
							.request(app)
							.put(`/api/v1/user/${sampleUser._id}`)
							.send({
								nom: "jojo",
								prenom: "bizzare",
								email: "adventure@gmail.fr",
							})
							.set("Cookie", `t=${token}`)
							.end((err, res) => {
								if (err) {
									throw err;
								}
								res.should.have.status(200);
								res.body.should.be.a("object");
								res.body.should.have.property("message");
								res.body.should.have.property("user");
								done();
							});
					});
			});
			it("ne doit pas supprimer un utilisateur si je ne suis pas admin", (done) => {
				chai
					.request(app)
					.put("/api/v1/user/606aed0f55a7572b9c925f21")
					.send({
						nom: "jojo",
						prenom: "bizzare",
						email: "adventure@gmail.fr",
					})
					.end((err, res) => {
						if (err) {
							throw err;
						}
						res.should.have.status(401);
						res.body.should.be.a("object");
						res.body.should.have.property("message");
						done();
					});
			});
		});
	});

	after(async function () {
		disconnect();
	});
});
