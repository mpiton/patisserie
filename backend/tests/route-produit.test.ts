import chai from "chai";
import chaiHttp from "chai-http";
import { connect, disconnect } from "../config/db";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

let token: string;
// Test CRUD Produits
describe("CRUD Produits", () => {
	before(async function () {
		await connect();
	});
	//Test GET api/v1/produits
	describe("GET ALL Produits", () => {
		it("doit récupérer tous les produits", (done) => {
			chai
				.request(app)
				.get("/api/v1/produits")
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(200);
					res.body.should.be.a("object");
					res.body.produits.should.be.an("array");
					res.body.produits[0].should.have.property("nom");
					res.body.produits[0].should.have.property("slug");
					res.body.produits[0].should.have.property("isBest");
					res.body.produits[0].should.have.property("categories");
					res.body.produits[0].should.have.property("prix");
					res.body.produits[0].should.have.property("description");
					done();
				});
		});
	});
	// GET /api/v1/produit/:id
	describe("/GET/:id Produit", () => {
		it("doit récupérer un produit par son ID", (done) => {
			chai
				.request(app)
				.get("/api/v1/produit/606dc07510953552345b4f87")
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(200);
					res.body.should.be.a("object");
					res.body.produit.should.have.property("nom");
					res.body.produit.should.have.property("slug");
					res.body.produit.should.have.property("isBest");
					res.body.produit.should.have.property("categories");
					res.body.produit.should.have.property("prix");
					res.body.produit.should.have.property("description");
					res.body.produit.should.have.property("image");
					done();
				});
		});
		it("ne doit rien récupérer si l'ID est invalide", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.get(`/api/v1/produit/123456`)
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test DELETE api/v1/produit/:id
	describe("DELETE Produit par son ID", () => {
		it("doit pouvoir supprimer un produit si je suis Admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/produit/606dc0a410953552345b4f89")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("produit");
							done();
						});
				});
		});
		it("ne doit pas supprimer un produit si je ne suis pas admin", (done) => {
			const sampleUser = {
				email: "test@gmail.fr",
				password: "azerty123",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.delete("/api/v1/produit/606dc0a410953552345b4f89")
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(401);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							done();
						});
				});
		});
	});
	//Test PUT api/v1/produit/:id
	describe("PUT Produit par son ID", () => {
		it("doit pouvoir update un produit si je suis admin", (done) => {
			const sampleUser = {
				email: "mathieu@piton2.fr",
				password: "azerty",
			};
			chai
				.request(app)
				.post("/api/v1/login")
				.send(sampleUser)
				.then((res) => {
					token = res.body.token;
					chai
						.request(app)
						.put(`/api/v1/produit/606dc0a410953552345b4f89`)
						.send({
							nom: "Tiramisu",
						})
						.set("Cookie", `t=${token}`)
						.end((err, res) => {
							if (err) {
								throw err;
							}
							res.should.have.status(200);
							res.body.should.be.a("object");
							res.body.should.have.property("message");
							res.body.should.have.property("produit");
							done();
						});
				});
		});
		it("ne doit pas update un produit si je ne suis pas admin", (done) => {
			chai
				.request(app)
				.put("/api/v1/produit/606dc0a410953552345b4f89")
				.send({
					nom: "tarte aux pommes",
				})
				.end((err, res) => {
					if (err) {
						throw err;
					}
					res.should.have.status(401);
					res.body.should.be.a("object");
					res.body.should.have.property("message");
					done();
				});
		});
	});

	after(async function () {
		disconnect();
	});
});
