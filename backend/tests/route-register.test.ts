import request from "supertest";
import chaiHttp from "chai-http";
import chai from "chai";
import app from "../server";

// Configure chai
chai.use(chaiHttp);

describe("Enregistrement d'un utilisateur", () => {
	it("doit retourner un 201 si les champs sont valides", (done) => {
		const sampleUser = {
			prenom: "Mathieu",
			nom: "Piton",
			email: "john-doe@gmail.com",
			password: "Secret!23",
		};
		//Envoi de la requête vers l'API
		request(app)
			.post("/api/v1/user")
			.send(sampleUser)
			.then((res) => {
				//assertion
				res.should.have.status(201);
				res.body.should.be.a("object");
				res.body.should.have.property("prenom").equal(sampleUser.prenom);
				res.body.should.have.property("nom").equal(sampleUser.nom);
				res.body.should.have.property("email").equal(sampleUser.email);
				res.body.should.have.property("hashed_password").equal("privé");
				res.body.should.have.property("salt").equal("privé");
				done();
			})
			.catch((err) => {
				console.log(err.message);
			});
	});
	it("doit retourner un 401 si l'email n'est pas valide", (done) => {
		const sampleUser = {
			prenom: "Mathieu",
			nom: "Piton",
			email: "john-doe@gmail.com<br>",
			password: "Secret23!",
		};

		//Envoi de la requête vers l'API
		request(app)
			.post("/api/v1/user")
			.send(sampleUser)
			.then((res) => {
				//assertion
				res.should.have.status(401);
				res.body.should.be.a("object");
				res.body.should.have
					.property("message")
					.equal("Requête invalide: Erreur de validation");
				done();
			})
			.catch((err) => {
				console.log(err.message);
			});
	});
	it("doit retourner un 401 si le nom n'est pas valide", (done) => {
		const sampleUser = {
			prenom: "Mathieu",
			nom: "Piton!",
			email: "john-doe@gmail.com",
			password: "Secret23!",
		};

		//Envoi de la requête vers l'API
		request(app)
			.post("/api/v1/user")
			.send(sampleUser)
			.then((res) => {
				//assertion
				res.should.have.status(401);
				res.body.should.be.a("object");
				res.body.should.have
					.property("message")
					.equal("Requête invalide: Erreur de validation");
				done();
			})
			.catch((err) => {
				console.log(err.message);
			});
	});
	it("doit retourner un 401 si le prénom n'est pas valide", (done) => {
		const sampleUser = {
			prenom: "Mathieu!",
			nom: "Piton",
			email: "john-doe@gmail.com",
			password: "Secret23!",
		};

		//Envoi de la requête vers l'API
		request(app)
			.post("/api/v1/user")
			.send(sampleUser)
			.then((res) => {
				//assertion
				res.should.have.status(401);
				res.body.should.be.a("object");
				res.body.should.have
					.property("message")
					.equal("Requête invalide: Erreur de validation");
				done();
			})
			.catch((err) => {
				console.log(err.message);
			});
	});
	it("doit retourner un 401 si le mot de passe est trop court", (done) => {
		const sampleUser = {
			prenom: "Mathieu",
			nom: "Piton",
			email: "john-doe@gmail.com",
			password: "Se",
		};

		//Envoi de la requête vers l'API
		request(app)
			.post("/api/v1/user")
			.send(sampleUser)
			.then((res) => {
				//assertion
				res.should.have.status(401);
				res.body.should.be.a("object");
				res.body.should.have
					.property("message")
					.equal("Requête invalide: Erreur de validation");
				done();
			})
			.catch((err) => {
				console.log(err.message);
			});
	});
});
