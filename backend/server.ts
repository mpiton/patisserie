import cors from "cors";
import dotenv from "dotenv";
import express, { Application } from "express";
import morgan from "morgan";
import { connect } from "./config/db";
import categorieRoutes from "./routes/categorie";
import commandeRoutes from "./routes/commande";
import commentaireRoutes from "./routes/commentaire";
import headerRoutes from "./routes/header";
import panierRoutes from "./routes/panier";
import produitRoutes from "./routes/produit";
import resetPasswordRoutes from "./routes/reset-password";
import userRoutes from "./routes/user";

dotenv.config();
const app: Application = express();

// Connect Database
connect();

app.use(cors());
const port: string = process.env.PORT as string;

// Body parsing Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(morgan("dev"));

//Lien vers les routes
app.use("/api/v1", userRoutes);
app.use("/api/v1", categorieRoutes);
app.use("/api/v1", commentaireRoutes);
app.use("/api/v1", produitRoutes);
app.use("/api/v1", headerRoutes);
app.use("/api/v1", resetPasswordRoutes);
app.use("/api/v1", commandeRoutes);
app.use("/api/v1", panierRoutes);

try {
	app.listen(port, (): void => {
		console.log(`Connecté sur le port ${port} 👍`);
	});
} catch (error) {
	console.error(`Error occured: ${error.message} 😡`);
}

export default app;
