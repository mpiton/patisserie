import { model, Schema } from "mongoose";
import { IPanier } from "../types/panier";

export const panierSchema = new Schema<IPanier>(
	{
		productId: {
			type: Schema.Types.ObjectId,
			ref: "Produit",
			required: true,
		},
		quantity: {
			type: Number,
			required: true,
			min: [1, "Ce produit a obligatoirement plus de 0 produit"],
		},
	},
	{ timestamps: true }
);

const Panier = model<IPanier>("paniers", panierSchema);
export default Panier;
