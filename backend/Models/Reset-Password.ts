import { IResetPassword } from "../types/reset-password";
import { model, Schema } from "mongoose";

export const resetPasswordSchema = new Schema<IResetPassword>(
	{
		userId: {
			type: Schema.Types.ObjectId,
			required: true,
		},
		token: {
			type: String,
			unique: true,
			lowercase: true,
		},
	},
	{ timestamps: true }
);

const ResetPassword = model<IResetPassword>("reset-password", resetPasswordSchema);
export default ResetPassword;
