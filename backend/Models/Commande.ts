import { model, Schema } from "mongoose";
import { ICommande } from "../types/commande";

const commandeSchema: Schema<ICommande> = new Schema({
	produits: [
		{
			produit: {
				type: Schema.Types.ObjectId,
				ref: "Produit",
				required: true,
			},
			quantity: {
				type: Number,
				required: true,
			},
		},
	],
	montantTotal: {
		type: Number,
		required: true,
	},
	pickupDate: {
		type: Date,
		required: true,
	},
	//TODO: StripeSessionId: String,
	status: String,
	remarque: String,
});

const Commande = model<ICommande>("commandes", commandeSchema);
export default Commande;
