import { model, Schema } from "mongoose";
import { ICategorie } from "../types/categorie";

export const categorieSchema = new Schema<ICategorie>(
	{
		nom: {
			type: String,
			required: [true, "Le champs nom est obligatoire."],
		},
		slug: {
			type: String,
			unique: true,
			lowercase: true,
		},
	},
	{ timestamps: true }
);

const Categorie = model<ICategorie>("categories", categorieSchema);
export default Categorie;
