import { model, Schema } from "mongoose";
import { IProduit } from "../types/produit";

export const produitSchema: Schema<IProduit> = new Schema(
	{
		nom: {
			type: String,
			required: [true, "Le champs nom est obligatoire."],
		},
		description: {
			type: String,
			required: [true, "Le champs description est obligatoire."],
		},
		slug: {
			type: String,
			unique: true,
			lowercase: true,
		},
		isBest: {
			type: Boolean,
			default: false,
		},
		categories: {
			type: [Schema.Types.ObjectId],
			ref: "Catégorie",
		},
		image: {
			type: Object,
			required: true,
		},
		prix: {
			type: Schema.Types.Decimal128,
			required: [true, "Le champs prix est obligatoire."],
		},
	},
	{ timestamps: true }
);

produitSchema
	.virtual("imagePath")
	.get(function (this: { image: string; imageType: string }) {
		if (this.image != null && this.imageType != null) {
			return `data:${
				this.imageType
			};charset=utf-8;base64,${this.image.toString()}`;
		}
	});

const Produit = model<IProduit>("produits", produitSchema);
export default Produit;
