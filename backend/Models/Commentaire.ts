import { model, Schema } from "mongoose";
import { ICommentaire } from "../types/commentaire";

export const commentaireSchema = new Schema<ICommentaire>(
	{
		titre: {
			required: [true, "Le champs titre est obligatoire."],
			type: String,
		},
		createdBy: {
			type: String,
			required: [true, "Ce commentaire a obligatoirement un créateur."],
			lowercase: true,
		},
		commentaire: {
			type: String,
			required: [true, "Ce champ est obligatoire."],
		},
		note: {
			type: Number,
			required: [true, "Ce champ est obligatoire."],
			min: [0, "La note ne peut être inférieur à 0."],
			max: [5, "La note ne peut être supérieure à 5."],
		},
		productId: {
			type: Schema.Types.ObjectId,
			ref: "Produit",
			required: [true, "Ce commentaire est lié à un produit"],
		},
	},
	{ timestamps: true }
);

const Commentaire = model<ICommentaire>("commentaires", commentaireSchema);
export default Commentaire;
