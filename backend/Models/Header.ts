import { model, Schema } from "mongoose";
import { IHeader } from "../types/header";

export const headerSchema = new Schema<IHeader>(
	{
		productId: {
			type: Schema.Types.ObjectId,
			ref: "Produit",
			required: [true, "Veuillez lier un produit à ce header."],
		},
	},
	{ timestamps: true }
);

const Header = model<IHeader>("headers", headerSchema);
export default Header;
