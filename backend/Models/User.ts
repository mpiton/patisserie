import * as bcrypt from "bcryptjs";
import { model, Schema } from "mongoose";
import { IUser } from "../types/users";

export const userSchema: Schema<IUser> = new Schema(
	{
		nom: {
			type: String,
			required: [true, "Le champs nom est obligatoire."],
		},

		prenom: {
			type: String,
			required: [true, "Le champs prénom est obligatoire."],
		},

		email: {
			type: String,
			required: [true, "Le champs email est obligatoire."],
			lowercase: true,
		},
		salt: String,
		hashed_password: {
			type: String,
			required: [true, "Le mot de passe est obligatoire."],
		},
		role: {
			type: [String],
			default: [],
		},
		panier: [
			{
				productId: {
					type: Schema.Types.ObjectId,
					ref: "Panier",
					required: true,
				},
				quantity: {
					type: Number,
					required: true,
				},
			},
		],
		commandes: [
			{
				type: Schema.Types.ObjectId,
				ref: "Commande",
			},
		],
	},
	{ timestamps: true }
);

userSchema
	.virtual("password")
	.get(function (this: { _password: string }) {
		return this._password;
	})
	.set(function (
		this: { _password: string; salt: string; hashed_password: string },
		password: string
	) {
		this._password = password;
		const salt = (this.salt = bcrypt.genSaltSync(10));
		this.hashed_password = bcrypt.hashSync(password, salt);
	});

userSchema.methods.comparePasswords = function (
	candidatePassword: string,
	next: (err: Error | null, same: boolean | null) => void
) {
	bcrypt.compare(candidatePassword, this.hashed_password, (err, isMatch) => {
		if (err) {
			return next(err, null);
		}
		next(null, isMatch);
	});
};

const User = model<IUser>("users", userSchema);
export default User;
