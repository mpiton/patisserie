import dotenv from "dotenv";
import { Request, Response } from "express";
import Joi from "joi";
import * as jwt from "jsonwebtoken";
import Commande from "../Models/Commande";
import Produit from "../Models/Produit";
import { IProduit } from "../types/produit";
import User from "./../Models/User";
import { ICommande } from "./../types/commande";
import { IPanier } from "./../types/panier";

dotenv.config();

//Pattern de validation
const schema = Joi.object().keys({
	pickupDate: Joi.date()
		.greater("now")
		.messages({
			"date.base": `Ce champ doit être de type Date`,
			"date.empty": `Ce champs est obligatoire`,
			"date.greater": `La date doit être supérieur à la date d'aujourd'hui`,
			"any.required": `La date est requise pour faire la commande`,
		})
		.required(),
	produits: Joi.array().items(
		Joi.object({
			productId: Joi.string().lowercase().required().messages({
				"string.base": `Ce champ doit être une chaîne de caractère`,
				"string.empty": `Ce champs est obligatoire`,
				"string.lowercase": `Ce champ ne peut pas avoir de majuscule`,
				"any.required": `La commande doit obligatoirement avoir un produit lié à sa quantité`,
			}),
			quantity: Joi.number().min(1).messages({
				"number.base": `Ce champ doit être de type Number`,
				"number.empty": `Ce champs est obligatoire`,
				"number.min": `La quantité ne peut être inférieur à 1`,
				"any.required": `La quantité est requise pour faire la commande`,
			}),
		})
	),
});

// Création d'une commande
const addCommande = async (req: Request, res: Response): Promise<void> => {
	try {
		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			let cookie: string = req.headers.cookie as string;
			const secret: string = process.env.JWT_SECRET as string;

			if (!cookie || cookie === undefined) {
				res.status(401).json("Vous devez vous connecter");
			} else {
				cookie = cookie.replace("t=", "");
			}
			const result: { _id: string; role: string[] } = <
				{ _id: string; role: string[] }
			>jwt.verify(cookie, secret);
			if (result) {
				//récupére le client
				const client = await User.findById(result._id);
				//récupére la commande
				const panier = <IPanier[]>[...req.body.produits];
				const produitsDuPanier: { produit: IProduit; quantity: number }[] = [];
				let montantNet = 0.0;

				//calcule du montant
				const produits = await Produit.find()
					.where("_id")
					.in(panier.map((produit) => produit.productId))
					.exec();
				produits.map((produit) => {
					panier.map(({ productId, quantity }) => {
						if (produit._id == productId) {
							montantNet +=
								parseFloat(produit.prix as unknown as string) * quantity;
							produitsDuPanier.push({ produit, quantity });
						}
					});
				});

				//Création de la commande avec la description des produits
				if (produitsDuPanier) {
					const maDate = req.body.pickupDate as Date;
					const maCommande = await Commande.create({
						produits: produitsDuPanier,
						montantTotal: montantNet,
						pickupDate: maDate,
						status: "En attente",
						remarque: "",
					});
					//J'attache ma commande à mon client
					if (maCommande && client) {
						client.panier = []; //vide le panier
						client.commandes.push(maCommande);
						await client.save();
						//retourne la commande
						res.status(201).json(maCommande);
					}
				}
			}
		} else {
			res.status(401).json({
				message: error,
				data: req.body,
			});
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

//GET ALL Commandes d'un client par son cookie
const getAllCommandes = async (req: Request, res: Response): Promise<void> => {
	try {
		let cookie: string = req.headers.cookie as string;
		const secret: string = process.env.JWT_SECRET as string;

		if (!cookie || cookie === undefined) {
			res.status(401).json("Vous devez vous connecter");
		} else {
			cookie = cookie.replace("t=", "");
		}
		const result: { _id: string; role: string[] } = <
			{ _id: string; role: string[] }
		>jwt.verify(cookie, secret);
		if (result) {
			//récupére le client

			const client = await User.findById(result._id).populate("Commandes");
			if (client) {
				res.status(200).json(client.commandes);
			}
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET Commande BY ID
const getCommandeById = async (req: Request, res: Response): Promise<void> => {
	try {
		const commande: ICommande = (
			await Commande.findById(req.params.id)
		)?.populate("produits.produit") as unknown as ICommande;
		if (!commande) {
			res.status(404).json({ error: "Commande non trouvé" });
		}
		res.status(200).json({ commande });
	} catch (error) {
		res.status(401).json({ message: "ID invalide" });
	}
};

// ** ADMIN
//GET ALL Commandes
const getAdminCommandes = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const commandes = await Commande.find();
		if (commandes === null) {
			res
				.status(404)
				.json({ message: "Impossible de trouver les commandes !" });
		} else {
			res.status(200).json(commandes);
		}
	} catch (error) {
		res.status(500).send(error);
	}
};
// UPDATE Commande
const putAdminCommandes = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		//Pattern de validation
		const putSchema = Joi.object().keys({
			pickupDate: Joi.date().greater("now").messages({
				"date.base": `Ce champ doit être de type Date`,
				"date.empty": `Ce champs est obligatoire`,
				"date.greater": `La date doit être supérieur à la date d'aujourd'hui`,
			}),
			status: Joi.string().required().messages({
				"string.base": `Ce champ doit être une chaîne de caractère`,
				"string.empty": `Ce champs est obligatoire`,
				"any.required": `La commande doit obligatoirement avoir un status`,
			}),
			remarque: Joi.string().messages({
				"string.base": `Ce champ doit être une chaîne de caractère`,
			}),
		});
		//Try validation
		const result = putSchema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			const commandeId = req.params.id;
			const { status, remarque, pickupDate } = req.body;
			const commande = await Commande.findById(commandeId).populate("Produit");
			if (commandeId && commande) {
				commande.status = status;
				commande.remarque = remarque;
				if (pickupDate) {
					commande.pickupDate = pickupDate;
				}
				const resultatCommande = await commande.save();
				if (resultatCommande === null) {
					res
						.status(400)
						.send({ message: "La mise à jour de la commande a échoué" });
				} else {
					res.status(200).json({
						message: "Commande mise à jour",
						commande: resultatCommande,
					});
				}
			} else {
				res.status(404).send({ message: "L'ID de la commande n'existe pas" });
			}
		} else {
			res.status(401).json({
				message: error,
				data: req.body,
			});
		}
	} catch (error) {
		res.status(401).send({ message: "Mise à jour impossible" });
	}
};

export {
	addCommande,
	getAdminCommandes,
	getAllCommandes,
	getCommandeById,
	putAdminCommandes,
};
