import { Request, Response } from "express";
import Joi from "joi";
import Header from "../Models/Header";
import { IHeader } from "../types/header";

//Pattern de validation
const schema = Joi.object().keys({
	productId: Joi.string().lowercase().min(2).max(50).required(),
});

// ADD HEADER
const addHeader = async (req: Request, res: Response): Promise<void> => {
	try {
		const { productId } = req.body;

		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			//Création du header
			const header = new Header({
				productId,
			});
			await header.save();
			res.status(201).json({
				message: "Header créé",
				header,
			});
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
				error,
			});
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET ALL Headers
const getHeaders = async (req: Request, res: Response): Promise<void> => {
	try {
		const headers: IHeader[] = await Header.find();
		if (!headers) {
			res.status(404).json({ message: "Aucun header trouvé" });
		}
		res.status(200).json({ headers });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET HEADER BY ID
const getHeaderById = async (req: Request, res: Response): Promise<void> => {
	try {
		const header: IHeader = (await Header.findById(req.params.id)) as IHeader;
		if (!header) {
			res.status(404).json({ error: "Header non trouvé" });
		}
		res.status(200).json({ header });
	} catch (error) {
		res.status(401).json({ message: "ID invalide" });
	}
};

// UPDATE Catégorie
const updateHeader = async (req: Request, res: Response): Promise<void> => {
	try {
		const {
			params: { id },
			body,
		} = req;
		const updateHeader: IHeader | null = await Header.findByIdAndUpdate(
			{ _id: id },
			{
				productId: body.productId,
			}
		);
		res.status(200).json({
			message: "Header à jour",
			header: updateHeader,
		});
	} catch (error) {
		res.status(401).send({ message: "Mise à jour impossible" });
	}
};

//DELETE Header
const deleteHeader = async (req: Request, res: Response): Promise<void> => {
	try {
		const deletedHeader: IHeader | null = await Header.findByIdAndRemove(
			req.params.id
		);
		res.status(200).json({
			message: "Header supprimé",
			header: deletedHeader,
		});
	} catch (error) {
		res.status(401).json({ message: "Vous ne pouvez pas faire ceci" });
	}
};

export { addHeader, deleteHeader, getHeaderById, getHeaders, updateHeader };
