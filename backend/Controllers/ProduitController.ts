import { Request, Response } from "express";
import Joi from "joi";
import Produit from "../Models/Produit";
import { IProduit } from "../types/produit";

//Pattern de validation
const schema = Joi.object().keys({
	nom: Joi.string().min(2).max(30).required(),
	slug: Joi.string().min(2).max(30),
	isBest: Joi.boolean(),
	categories: Joi.array().items(Joi.string().alphanum()),
	description: Joi.string().min(3).max(2000),
	prix: Joi.number().required(),
});

// ADD PRODUIT
const addProduit = async (req: Request, res: Response): Promise<void> => {
	try {
		const { nom, description, categories, prix } = req.body;

		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			//Création du produit
			const produit = new Produit({
				nom,
				slug: slugify(nom),
				categories,
				image: req.file,
				description,
				prix,
			});
			produit.save();
			res.status(201).json({
				message: "Produit créé",
				produit,
			});
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
				error,
			});
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET ALL Produits
const getProduits = async (req: Request, res: Response): Promise<void> => {
	try {
		const produits: IProduit[] = await Produit.find();
		if (!produits) {
			res.status(404).json({ message: "Aucun produit trouvé" });
		}
		res.status(200).json({ produits });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET PRODUIT BY ID
const getProduitById = async (req: Request, res: Response): Promise<void> => {
	try {
		const produit: IProduit = (await Produit.findById(
			req.params.produitId
		)) as IProduit;
		if (!produit) {
			res.status(404).json({ error: "Produit non trouvé" });
		}
		res.status(200).json({ produit });
	} catch (error) {
		res.status(401).json({ message: "ID invalide" });
	}
};

//GET PRODUITS BY NOM
const getProduitsByNom = async (req: Request, res: Response): Promise<void> => {
	try {
		const produits: IProduit[] = (await Produit.find({
			nom: req.params.produitNom,
		})) as IProduit[];
		if (!produits) {
			res.status(404).json({ error: "Produits non trouvées" });
		}
		res.status(200).json({ produits });
	} catch (error) {
		res.status(401).json({ message: "Nom invalide" });
	}
};

// UPDATE Catégorie
const updateProduit = async (req: Request, res: Response): Promise<void> => {
	try {
		const {
			params: { id },
			body,
		} = req;
		const slug = slugify(body.nom);
		const updateProduit: IProduit | null = await Produit.findByIdAndUpdate(
			{ _id: id },
			{
				nom: body.nom,
				slug,
				categories: body.categories,
				image: req.file,
				prix: body.prix,
				description: body.description,
			}
		);
		res.status(200).json({
			message: "Produit à jour",
			produit: updateProduit,
		});
	} catch (error) {
		res.status(401).send({ message: "Mise à jour impossible" });
	}
};

//DELETE Produit
const deleteProduit = async (req: Request, res: Response): Promise<void> => {
	try {
		const deletedProduit: IProduit | null = await Produit.findByIdAndRemove(
			req.params.id
		);
		res.status(200).json({
			message: "Produit supprimé",
			produit: deletedProduit,
		});
	} catch (error) {
		res.status(401).json({ message: "Vous ne pouvez pas faire ceci" });
	}
};

// Slugify a string
function slugify(str: string): string {
	str = str.replace(/^\s+|\s+$/g, "");

	// Make the string lowercase
	str = str.toLowerCase();

	// Remove accents, swap ñ for n, etc
	const from =
		"ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
	const to =
		"AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
	for (let i = 0, l = from.length; i < l; i++) {
		str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
	}

	// Remove invalid chars
	str = str
		.replace(/[^a-z0-9 -]/g, "")
		// Collapse whitespace and replace by -
		.replace(/\s+/g, "-")
		// Collapse dashes
		.replace(/-+/g, "-");

	return str;
}

export {
	addProduit,
	deleteProduit,
	getProduitById,
	getProduits,
	getProduitsByNom,
	updateProduit,
};
