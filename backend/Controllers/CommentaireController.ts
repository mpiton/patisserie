import dotenv from "dotenv";
import { Request, Response } from "express";
import Joi from "joi";
import jwt from "jsonwebtoken";
import Commentaire from "../Models/Commentaire";
import { ICommentaire } from "../types/commentaire";

dotenv.config();
const secret: string = process.env.JWT_SECRET as string;

//Pattern de validation
const schema = Joi.object().keys({
	titre: Joi.string().min(2).max(30).required(),
	commentaire: Joi.string().min(2).max(1000).required(),
	createdBy: Joi.string().lowercase().min(2).max(50).required(),
	note: Joi.number().integer().positive().max(5).required(),
	produitId: Joi.string().lowercase().min(2).max(50).required(),
});

// ADD COMMENTAIRE
const addCommentaire = async (req: Request, res: Response): Promise<void> => {
	//Assignation de l'ID Utilisateur au commentaire
	try {
		const { titre, commentaire, note } = await req.body;
		let cookie: string = req.headers.cookie as string;

		if (cookie) {
			cookie = cookie.replace("t=", "");
		}
		if (!cookie || cookie === undefined) {
			res.status(401).json({ message: "Vous devez vous connecter" });
		}

		const resultat: { _id: string; role: string[] } = <
			{ _id: string; role: string[] }
		>jwt.verify(cookie, secret);

		req.body.produitId = req.params.productId;
		req.body.createdBy = resultat._id;
		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			//Création du commentaire
			const myCommentaire = new Commentaire({
				titre,
				commentaire,
				produitId: req.body.produitId,
				createdBy: req.body.createdBy,
				note,
			});

			await myCommentaire.save();
			res.status(201).json({
				message: "Commentaire créé",
				myCommentaire,
			});
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
			});
		}
	} catch {
		res
			.status(401)
			.json({ message: "Vous n'êtes pas autorisé à poster un commentaire." });
	}
};

//GET ALL Commentaires
const getCommentaires = async (req: Request, res: Response): Promise<void> => {
	try {
		const commentaires: ICommentaire[] = await Commentaire.find();
		if (!commentaires) {
			res.status(404).json({ message: "Aucun commentaire trouvé" });
		}
		res.status(200).json({ commentaires });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET Commentaire BY ID
const getCommentaireById = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const commentaire: ICommentaire = (await Commentaire.findById(
			req.params.commentaireId
		)) as ICommentaire;
		if (!commentaire) {
			res.status(404).json({ error: "Commentaire non trouvé" });
		}
		res.status(200).json({ commentaire });
	} catch (error) {
		res.status(401).json({ message: "ID invalide" });
	}
};

// UPDATE Commentaire
const updateCommentaire = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const {
			params: { id },
			body,
		} = req;
		const updateCommentaire: ICommentaire | null =
			await Commentaire.findByIdAndUpdate({ _id: id }, body);
		res.status(200).json({
			message: "Commentaire à jour",
			commentaire: updateCommentaire,
		});
	} catch (error) {
		res.status(401).send({ message: "Mise à jour impossible" });
	}
};

//DELETE Commentaire
const deleteCommentaire = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const deletedCommentaire: ICommentaire | null =
			await Commentaire.findByIdAndRemove(req.params.id);
		res.status(200).json({
			message: "Commentaire supprimée",
			commentaire: deletedCommentaire,
		});
	} catch (error) {
		res.status(401).json({ message: "Vous ne pouvez pas faire ceci" });
	}
};

export {
	addCommentaire,
	deleteCommentaire,
	getCommentaireById,
	getCommentaires,
	updateCommentaire,
};
