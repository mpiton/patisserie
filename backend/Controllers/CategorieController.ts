import { Request, Response } from "express";
import Joi from "joi";
import Categorie from "../Models/Categorie";
import { ICategorie } from "../types/categorie";

//Pattern de validation
const schema = Joi.object().keys({
	nom: Joi.string().min(2).max(30).required(),
	slug: Joi.string().min(2).max(30),
});

// ADD CATEGORIE
const addCategorie = async (req: Request, res: Response): Promise<void> => {
	try {
		const { nom } = req.body;
		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			//Création de la catégorie
			const categorie = new Categorie({ nom, slug: slugify(nom) });
			await categorie.save();
			res.status(201).json({
				message: "Catégorie créée",
				categorie,
			});
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
			});
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET ALL Catégories
const getCategories = async (req: Request, res: Response): Promise<void> => {
	try {
		const categories: ICategorie[] = await Categorie.find();
		if (!categories) {
			res.status(404).json({ message: "Aucune catégorie trouvée" });
		}
		res.status(200).json({ categories });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET CATEGORY BY ID
const getCategorieById = async (req: Request, res: Response): Promise<void> => {
	try {
		const categorie: ICategorie = (await Categorie.findById(
			req.params.categorieId
		)) as ICategorie;
		if (!categorie) {
			res.status(404).json({ error: "Catégorie non trouvée" });
		}
		res.status(200).json({ categorie });
	} catch (error) {
		res.status(401).json({ message: "ID invalide" });
	}
};

//GET CATEGORIES BY NOM
const getCategoriesByNom = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		const categories: ICategorie[] = (await Categorie.find({
			nom: req.params.categorieNom,
		})) as ICategorie[];
		if (!categories) {
			res.status(404).json({ error: "Catégories non trouvées" });
		}
		res.status(200).json({ categories });
	} catch (error) {
		res.status(401).json({ message: "Nom invalide" });
	}
};

// UPDATE Catégorie
const updateCategorie = async (req: Request, res: Response): Promise<void> => {
	try {
		const {
			params: { id },
			body,
		} = req;
		const slug = slugify(body.nom);
		const updateCategorie: ICategorie | null =
			await Categorie.findByIdAndUpdate({ _id: id }, { nom: body.nom, slug });
		res.status(200).json({
			message: "Catégorie mise à jour",
			categorie: updateCategorie,
		});
	} catch (error) {
		res.status(401).send({ message: "Mise à jour impossible" });
	}
};

//DELETE Category
const deleteCategorie = async (req: Request, res: Response): Promise<void> => {
	try {
		const deletedCategorie: ICategorie | null =
			await Categorie.findByIdAndRemove(req.params.id);
		res.status(200).json({
			message: "Catégorie supprimée",
			categorie: deletedCategorie,
		});
	} catch (error) {
		res.status(401).json({ message: "Vous ne pouvez pas faire ceci" });
	}
};

// Slugify a string
function slugify(str: string): string {
	str = str.replace(/^\s+|\s+$/g, "");

	// Make the string lowercase
	str = str.toLowerCase();

	// Remove accents, swap ñ for n, etc
	const from =
		"ÁÄÂÀÃÅČÇĆĎÉĚËÈÊẼĔȆÍÌÎÏŇÑÓÖÒÔÕØŘŔŠŤÚŮÜÙÛÝŸŽáäâàãåčçćďéěëèêẽĕȇíìîïňñóöòôõøðřŕšťúůüùûýÿžþÞĐđßÆa·/_,:;";
	const to =
		"AAAAAACCCDEEEEEEEEIIIINNOOOOOORRSTUUUUUYYZaaaaaacccdeeeeeeeeiiiinnooooooorrstuuuuuyyzbBDdBAa------";
	for (let i = 0, l = from.length; i < l; i++) {
		str = str.replace(new RegExp(from.charAt(i), "g"), to.charAt(i));
	}

	// Remove invalid chars
	str = str
		.replace(/[^a-z0-9 -]/g, "")
		// Collapse whitespace and replace by -
		.replace(/\s+/g, "-")
		// Collapse dashes
		.replace(/-+/g, "-");

	return str;
}

export {
	addCategorie,
	deleteCategorie,
	getCategorieById,
	getCategories,
	getCategoriesByNom,
	updateCategorie,
};
