import dotenv from "dotenv";
import { Request, Response } from "express";
import Joi from "joi";
import * as jwt from "jsonwebtoken";
import Produit from "../Models/Produit";
import User from "./../Models/User";
import { IPanier } from "./../types/panier";

dotenv.config();

//Pattern de validation
const schema = Joi.object().keys({
	productId: Joi.string().lowercase().alphanum().required().messages({
		"string.base": `Ce champ doit être une chaine de caractère`,
		"string.empty": `Ce champs est obligatoire`,
		"string.lowercase": `Le champs ne doit pas porter de majuscule`,
		"any.required": `Le produit est requis pour l'ajouter au panier`,
	}),
	quantity: Joi.number().min(1).messages({
		"number.base": `Ce champ doit être de type Number`,
		"number.empty": `Ce champs est obligatoire`,
		"number.min": `La quantité ne peut être inférieur à 1`,
		"any.required": `La quantité est requise pour faire la commande`,
	}),
});

// Ajout d'un produit au panier
const addIntoPanier = async (req: Request, res: Response): Promise<void> => {
	try {
		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			let cookie: string = req.headers.cookie as string;
			const secret: string = process.env.JWT_SECRET as string;

			if (!cookie || cookie === undefined) {
				res.status(401).json("Vous devez vous connecter");
			} else {
				cookie = cookie.replace("t=", "");
			}
			const result: { _id: string; role: string[] } = <
				{ _id: string; role: string[] }
			>jwt.verify(cookie, secret);
			if (result) {
				//récupére le client
				const client = await User.findById(result._id).populate(
					"panier.produit"
				);
				let produitsDansPanier: Array<IPanier> = [];
				const request = <IPanier>req.body;
				const produit = await Produit.findById(request.productId);
				if (produit && client !== null) {
					produitsDansPanier = client.panier;
					if (produitsDansPanier.length > 0) {
						const existProduit = produitsDansPanier.filter(
							(produit) => produit.productId === request.productId
						);
						if (existProduit.length > 0) {
							const index = produitsDansPanier.indexOf(existProduit[0]);
							if (request.quantity > 0) {
								produitsDansPanier[index] = request;
							} else {
								produitsDansPanier.splice(index, 1);
							}
						} else {
							produitsDansPanier.push(request);
						}
					} else {
						produitsDansPanier.push(request);
					}
					if (produitsDansPanier) {
						client.panier = produitsDansPanier;
						const resultatPanier = await client.save();
						res.status(200).send(resultatPanier.panier);
					}
				}
				res.status(400).json({ message: "Impossible de créer le panier !" });
			} else {
				res
					.status(401)
					.json({ message: "Impossible de retrouver l'utilisateur" });
			}
		} else {
			res.status(401).json({
				message: error,
				data: req.body,
			});
		}
	} catch (err) {
		res.status(500).json(err);
	}
};

//GET Panier de l'utilisateur
const getPanier = async (req: Request, res: Response): Promise<void> => {
	try {
		let cookie: string = req.headers.cookie as string;
		const secret: string = process.env.JWT_SECRET as string;

		if (!cookie || cookie === undefined) {
			res.status(401).json("Vous devez vous connecter");
		} else {
			cookie = cookie.replace("t=", "");
		}
		const result: { _id: string; role: string[] } = <
			{ _id: string; role: string[] }
		>jwt.verify(cookie, secret);
		if (result) {
			//récupére le client

			const client = await User.findById(result._id).populate(
				"Panier.productId"
			);
			if (client) {
				res.status(200).json(client.panier);
			}
			res.status(400).json({ message: "Le panier n'a pas été trouvé" });
		}
		res
			.status(401)
			.json({ message: "Le cookie de l'utilisateur n'a pas été trouvé" });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET Commande BY ID
const deletePanier = async (req: Request, res: Response): Promise<void> => {
	try {
		let cookie: string = req.headers.cookie as string;
		const secret: string = process.env.JWT_SECRET as string;

		if (!cookie || cookie === undefined) {
			res.status(401).json("Vous devez vous connecter");
		} else {
			cookie = cookie.replace("t=", "");
		}
		const result: { _id: string; role: string[] } = <
			{ _id: string; role: string[] }
		>jwt.verify(cookie, secret);
		if (result) {
			//récupére le client
			const client = await User.findById(result._id).populate(
				"Panier.productId"
			);
			if (client === null) {
				res.status(400).json({ message: "Le panier est déjà vide" });
			} else {
				client.panier = [];
				const resulatPanier = await client.save();
				client.hashed_password = "privé";
				client.salt = "privé";
				res.status(200).json(resulatPanier);
			}
		} else {
			res
				.status(401)
				.json({ message: "Le cookie de l'utilisateur n'a pas été trouvé" });
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

export { addIntoPanier, deletePanier, getPanier };
