import { Request, Response } from "express";
import Joi from "joi";
import * as jwt from "jsonwebtoken";
import User from "../Models/User";
import { IUser } from "../types/users";

//Pattern de validation
const schema = Joi.object().keys({
	nom: Joi.string().alphanum().min(3).max(30).required(),
	prenom: Joi.string().alphanum().min(3).max(30).required(),
	email: Joi.string().email().required(),
	role: Joi.array().items(Joi.string().valid("USER", "ADMIN")),
	password: Joi.string().pattern(
		new RegExp("^[0-9a-zA-Z.!@$%^&(){}:;,.?/~_+-=|]{8,40}$")
	),
});

//GET ALL USERS
const getUsers = async (_req: Request, res: Response): Promise<void> => {
	try {
		const users: IUser[] = await User.find();
		if (!users) {
			res.status(404).json({ message: "Aucun utilisateur trouvé" });
		}
		//cache les salt et passwords
		users.forEach((user) => {
			user.hashed_password = "privé";
			user.salt = "privé";
		});
		res.status(200).json({ users });
	} catch (error) {
		res.status(500).send(error);
	}
};

const getUserById = async (req: Request, res: Response): Promise<void> => {
	try {
		const user: IUser = (await User.findById(req.params.userId)) as IUser;
		if (!user) {
			res.status(404).json({ error: "Utilisateur non trouvé" });
		}
		user.hashed_password = "privé";
		user.salt = "privé";
		res.status(200).json({ user });
	} catch (error) {
		res.status(401).json({ message: "ID invalide" });
	}
};

// REGISTER USER
const register = async (req: Request, res: Response): Promise<void> => {
	try {
		const { nom, prenom, email, password } = req.body;
		//Try validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			//Création de l'utilisateur
			const user = new User({ nom, prenom, email, password });
			await user.save();
			res.status(201).json({
				message: "Utilisateur créé",
				user,
			});
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
			});
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

// UPDATE USER
const updateUser = async (req: Request, res: Response): Promise<void> => {
	try {
		const {
			params: { id },
			body,
		} = req;
		const updateUser: IUser | null = await User.findByIdAndUpdate(
			{ _id: id },
			body
		);
		if (updateUser !== null) {
			updateUser.hashed_password = "privé";
			updateUser.salt = "privé";
		}
		res.status(200).json({
			message: "Utilisateur à jour",
			user: updateUser,
		});
	} catch (error) {
		res.status(401).send({ message: "Mise à jour impossible" });
	}
};

//DELETE USER
const deleteUser = async (req: Request, res: Response): Promise<void> => {
	try {
		const deletedUser: IUser | null = await User.findByIdAndRemove(
			req.params.id
		);
		const allUsers: IUser[] = await User.find();
		res.status(200).json({
			message: "Utilisateur supprimé",
			user: deletedUser,
			users: allUsers,
		});
	} catch (error) {
		res.status(401).json({ message: "Vous ne pouvez pas faire ceci" });
	}
};

const login = async (req: Request, res: Response): Promise<void> => {
	User.findOne({ email: req.body.email }, (error: Error, user: IUser) => {
		if (error || !user) {
			return res.json({ error: "Utilisateur non trouvé" });
		}
		user.comparePasswords(
			req.body.password,
			async (error: Error, isMatch: boolean): Promise<void> => {
				if (error) {
					return console.log(error);
				}
				if (!isMatch) {
					res.status(401).json({ error: "Email et mot de passe incorrects !" });
				}
				const secret = process.env.JWT_SECRET as string;
				const token = jwt.sign({ _id: user._id, role: user.role }, secret);
				res.cookie("t", token, {
					maxAge: 2_592_000,
				});
				user.hashed_password = "privé";
				user.salt = "privé";
				res.status(202).json({
					token,
					user,
				});
			}
		);
	});
};

//Logout
const logout = async (_req: Request, res: Response): Promise<void> => {
	try {
		res.clearCookie("t");
		res.json({ message: "Déconnecté" });
	} catch (error) {
		res.status(500).send(error);
	}
};

export {
	deleteUser,
	getUserById,
	getUsers,
	login,
	logout,
	register,
	updateUser,
};
