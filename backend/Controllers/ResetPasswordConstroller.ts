import * as bcrypt from "bcryptjs";
import { Request, Response } from "express";
import Joi from "joi";
import * as jwt from "jsonwebtoken";
import ResetPassword from "../Models/Reset-Password";
import User from "../Models/User";
import { IResetPassword } from "../types/reset-password";
import { IUser } from "../types/users";

//Pattern de validation
const schema = Joi.object().keys({
	id: Joi.string().lowercase().min(2).max(50).required(),
	token: Joi.string().regex(
		/^[A-Za-z0-9-_]+\.[A-Za-z0-9-_]+\.[A-Za-z0-9-_.+/=]*$/
	),
	password: Joi.string().pattern(
		new RegExp("^[0-9a-zA-Z.!@$%^&(){}:;,.?/~_+-=|]{8,40}$")
	),
});

// ADD RESETPASSWORD -> req.body.email
const addResetPassword = async (req: Request, res: Response): Promise<void> => {
	try {
		const schemaEmail = Joi.object().keys({
			email: Joi.string().email().required(),
		});
		const { email } = req.body;

		//Try validation
		const result = schemaEmail.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			// Recherche de l'utilisateur en BDD
			let payload: { id: string; email: string };
			const user = await User.findOne({ email });
			if (user) {
				payload = {
					id: user._id,
					email,
				};
				// ** Création d'un token unique utilisable une fois
				// Le password hashé en BDD avec la date de création de l'utilisateur
				const secret = `${user.hashed_password}-${user.createdAt.getTime()}`;
				const token = jwt.sign(payload, secret);

				//Création en BDD de la demande de reset
				const resetpassword = new ResetPassword({ userId: user.id, token });
				resetpassword.save();
				// TODO: Envoi d'un email contenant le lien du reset-password
				// Pour le moment je renvoi qu'une reponse en html
				res.send(
					'<a href="/resetpassword/' +
						payload.id +
						"/" +
						token +
						'">Reset password</a>'
				);
			} else {
				res.status(404).json({ message: "Cet email n'existe pas" });
			}
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
			});
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET ALL Catégories
const getResetPasswords = async (
	_req: Request,
	res: Response
): Promise<void> => {
	try {
		const resetpasswords: IResetPassword[] = await ResetPassword.find();
		if (!resetpasswords) {
			res.status(404).json({ message: "Aucune catégorie trouvée" });
		}
		res.status(200).json({ resetpasswords });
	} catch (error) {
		res.status(500).send(error);
	}
};

//GET CATEGORY BY ID && Token
const getResetPasswordById = async (
	req: Request,
	res: Response
): Promise<void> => {
	try {
		// ** Récupération de l'utilisateur en BDD
		const user: IUser = (await User.findById(req.params.id)) as IUser;
		if (!user) {
			res.status(404).json({ error: "Utilisateur non trouvé" });
		}
		// ** Decode le token grâce au données utilisateur
		// hashed_password + date de création de l'user
		const secret = `${user.hashed_password}-${user.createdAt.getTime()}`;
		const payload: { id: string; email: string } = <
			{ id: string; email: string }
		>jwt.verify(req.params.token, secret);

		// Si les données sont invalide
		if (payload.email != user.email || payload.id != user._id) {
			// renvoi un 401
			res.status(401).json({ message: "Unauthorized" });
		} else {
			//** Renvoi les données à mettre dans les champs caché du formulaire
			res.status(200).json({ _id: payload.id, token: req.params.token });
		}
	} catch (error) {
		res.status(401).json({ error });
	}
};

// Change Password
// Besoin de req.body > new password + user id (caché) + token (caché)
const changePassword = async (req: Request, res: Response): Promise<void> => {
	try {
		//**Validation
		const result = schema.validate(req.body);
		const { error } = result;
		const valid = error == null;

		if (valid) {
			// ** Récupération de l'utilisateur et de la demande de reset en BDD
			const user = await User.findOne({ _id: req.body.id });
			const reset = await ResetPassword.findOne({ token: req.body.token });
			if (!user || !reset) {
				res
					.status(404)
					.json({ message: "Utilisateur ou le token n'a pas été trouvé" });
			} else {
				const token: string = req.body.token;
				const resetUserId: string = req.body.id;
				const userId: string = user._id;

				// Si les données ne correspondent pas
				if (resetUserId != userId || reset.token != token) {
					// renvoi un 401
					res.status(401).json({ message: "Unauthorized" });
				} else {
					try {
						//** Hash du nouveau mot de passe et MAJ */
						const hash = await bcrypt.hash(req.body.password, Number(10));
						await User.updateOne(
							{ _id: userId },
							{ $set: { hashed_password: hash } },
							{ new: true }
						);
						//TODO: Envoi d'email pour confirmer le changement de mot de passe
						res.status(200).json({
							message: "Utilisateur à jour",
						});
					} catch (error) {
						res.status(401).send({ message: "Mise à jour impossible" });
					}
				}
			}
		} else {
			res.status(401).json({
				message: "Requête invalide: Erreur de validation",
				data: req.body,
				error,
			});
		}
	} catch (error) {
		res.status(500).send(error);
	}
};

export {
	addResetPassword,
	changePassword,
	getResetPasswordById,
	getResetPasswords,
};
