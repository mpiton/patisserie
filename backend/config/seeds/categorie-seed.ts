import { faker } from "@faker-js/faker";
import categorieSchema from "../../Models/Categorie";
import { ICategorie } from "../../types/categorie";
import { connect, disconnect } from "../db";

async function seedCategories() {
	connect();
	const categories = [] as ICategorie[];
	for (let index = 0; index < 10; index++) {
		const categorie = {
			nom: faker.commerce.department(),
			slug: faker.lorem.slug(),
		} as ICategorie;
		categories.push(categorie);
	}

	try {
		for (const categorie of categories) {
			await categorieSchema.create(categorie);
			console.log(`Created category ${categorie.nom}`);
		}
		disconnect();
	} catch (e) {
		console.error(e);
	}
}

seedCategories();
