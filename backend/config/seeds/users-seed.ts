import { faker } from "@faker-js/faker";
import userSchema from "../../Models/User";
import { IUser } from "../../types/users";
import { connect, disconnect } from "../db";

(async () => {
	connect();
	const users = [] as IUser[];
	for (let index = 0; index < 10; index++) {
		const sampleUser = {
			_id: faker.datatype.uuid(),
			nom: faker.name.lastName(),
			prenom: faker.name.firstName(),
			email: faker.internet.email(),
			password: faker.internet.password(),
			role: ["user"],
			salt: faker.datatype.uuid(),
			hashed_password: faker.datatype.uuid(),
			createdAt: faker.date.past(),
			commandes: [],
			panier: [],
		} as unknown as IUser;
		users.push(sampleUser);
	}

	try {
		for (const user of users) {
			await userSchema.create(user);
			console.log(`Created user ${user.nom} ${user.prenom}`);
		}
		disconnect();
	} catch (e) {
		console.error(e);
	}
})();
