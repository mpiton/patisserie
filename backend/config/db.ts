import dotenv from "dotenv";
import Mongoose from "mongoose";
import path from "path";

dotenv.config({ path: path.resolve(__dirname, "../.env") });

let database: Mongoose.Connection;

export const connect = async (): Promise<void> => {
	if (database) {
		return;
	}
	const db = process.env.MONGO_URI;
	if (!db) {
		console.error("Mongo URI is not defined 😡");
		process.exit(1);
	}

	try {
		await Mongoose.connect(db);
		console.log("MongoDB Connected... 😎");
	} catch (err) {
		console.error(err.message);
		process.exit(1);
	}
};

export const disconnect = (): void => {
	if (!database) {
		return;
	}
	Mongoose.disconnect();
};
