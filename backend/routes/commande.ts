import { Router } from "express";
import {
	addCommande,
	getAdminCommandes,
	getAllCommandes,
	getCommandeById,
	putAdminCommandes,
} from "../Controllers/CommandeController";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const router: Router = Router();

//** Commandes
//client
router.get("/commandes", checkRole([]), getAllCommandes); //récupération de toutes les commandes du client
router.post("/commande", checkRole([]), addCommande); //création de la commande
router.get("/commande/:id", checkRole([]), getCommandeById); //récupération d'une commande par son ID

//vendeur
router.get("/admin/commandes", checkRole([Role.Admin]), getAdminCommandes); //récupération de toutes les commandes
router.put("/admin/commande/:id", checkRole([Role.Admin]), putAdminCommandes); //gestion de la commande
export default router;
