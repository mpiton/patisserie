import { Router } from "express";
import {
	addCategorie,
	deleteCategorie,
	getCategorieById,
	getCategories,
	getCategoriesByNom,
	updateCategorie,
} from "../Controllers/CategorieController";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const router: Router = Router();

//CATEGORIE
router.get("/categories", checkRole([Role.Admin]), getCategories);
router.post("/categorie", checkRole([Role.Admin]), addCategorie);
router.put("/categorie/:id", checkRole([Role.Admin]), updateCategorie);
router.get("/categorie/:categorieId", getCategorieById);
router.get("/categorie/nom/:categorieNom", getCategoriesByNom);
router.delete("/categorie/:id", checkRole([Role.Admin]), deleteCategorie);

//categorie
router.param("categorieId", getCategorieById);
router.param("categorieNom", getCategoriesByNom);

export default router;
