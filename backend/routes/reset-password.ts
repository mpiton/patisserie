import { Router } from "express";
import {
	addResetPassword,
	changePassword,
	getResetPasswordById,
	getResetPasswords,
} from "../Controllers/ResetPasswordConstroller";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const router: Router = Router();

//RESET PASSWORD
router.get("/reset-password", checkRole([Role.Admin]), getResetPasswords); //affiche toutes les demande de reset-password
router.post("/reset-password", addResetPassword); //crée une demande de password
router.get("/reset-password/:id/:token", checkRole([]), getResetPasswordById); //affiche la demande de password
router.put("/change-password", changePassword); //Traite le changement de password

//reset-password
router.param("token", getResetPasswordById);

export default router;
