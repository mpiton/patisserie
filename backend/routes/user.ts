import { Router } from "express";
import {
	deleteUser,
	getUserById,
	getUsers,
	login,
	logout,
	register,
	updateUser,
} from "../Controllers/UserController";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const router: Router = Router();
//USERS
router.get("/users", checkRole([Role.Admin]), getUsers);
router.post("/user", register);
router.put("/user/:id", checkRole([]), updateUser);
router.get("/user/:userId", getUserById);
router.delete("/user/:id", checkRole([Role.Admin]), deleteUser);
router.post("/login", login);
router.post("/logout", checkRole([]), logout);

//PARAMS
//user
router.param("userId", getUserById);

export default router;
