import { Router } from "express";
import {
	addIntoPanier,
	deletePanier,
	getPanier,
} from "../Controllers/PanierController";
import { checkRole } from "../Middlewares/checkRole";

const router: Router = Router();

//PANIER
router.get("/panier", checkRole([]), getPanier);
router.post("/panier", checkRole([]), addIntoPanier);
router.delete("/panier", checkRole([]), deletePanier);

export default router;
