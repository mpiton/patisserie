import { Router } from "express";
import {
	addCommentaire,
	deleteCommentaire,
	getCommentaireById,
	getCommentaires,
	updateCommentaire,
} from "../Controllers/CommentaireController";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const router: Router = Router();

//COMMENTAIRES
router.get("/commentaires", checkRole([Role.Admin]), getCommentaires);
router.post("/commentaire/:productId", checkRole([]), addCommentaire);
router.put("/commentaire/:id", checkRole([Role.Admin]), updateCommentaire);
router.get("/commentaire/:commentaireId", getCommentaireById);
router.delete("/commentaire/:id", checkRole([Role.Admin]), deleteCommentaire);

//commentaire
router.param("commentaireId", getCommentaireById);
router.param("productId", addCommentaire);

export default router;
