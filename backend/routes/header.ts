import { Router } from "express";
import {
	addHeader,
	deleteHeader,
	getHeaderById,
	getHeaders,
	updateHeader,
} from "../Controllers/HeaderController";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const router: Router = Router();

//HEADERS
router.get("/headers", getHeaders);
router.post("/header", checkRole([Role.Admin]), addHeader);
router.put("/header/:id", checkRole([Role.Admin]), updateHeader);
router.get("/header/:id", getHeaderById);
router.delete("/header/:id", checkRole([Role.Admin]), deleteHeader);

export default router;
