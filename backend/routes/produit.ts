import { Router } from "express";
import multer from "multer";
import {
	addProduit,
	deleteProduit,
	getProduitById,
	getProduits,
	getProduitsByNom,
	updateProduit,
} from "../Controllers/ProduitController";
import { checkRole } from "../Middlewares/checkRole";
import Role from "../Middlewares/role";

const upload = multer({ dest: `./uploads` }); // apply filter

const router: Router = Router();

//PRODUITS
router.get("/produits", getProduits);
router.post(
	"/produit",
	upload.single("image"),
	checkRole([Role.Admin]),
	addProduit
);
router.put(
	"/produit/:id",
	upload.single("image"),
	checkRole([Role.Admin]),
	updateProduit
);
router.get("/produit/:produitId", getProduitById);
router.get("/produit/nom/:produitNom", getProduitsByNom);
router.delete("/produit/:id", checkRole([Role.Admin]), deleteProduit);

//produit
router.param("produitId", getProduitById);
router.param("produitNom", getProduitsByNom);

export default router;
