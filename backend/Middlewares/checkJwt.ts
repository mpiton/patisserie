import dotenv from "dotenv";
import { NextFunction, Request, Response } from "express";
import * as jwt from "jsonwebtoken";

dotenv.config();
const secret: string = process.env.JWT_SECRET as string;

export const checkJwt = async (
	req: Request,
	res: Response,
	next: NextFunction
): Promise<void> => {
	try {
		let cookie: string = req.headers.cookie as string;
		cookie = cookie.replace("t=", "");
		if (!cookie) {
			res.status(401).json("Vous devez vous connecter");
		}
		const result = jwt.verify(cookie, secret);
		if (!result) {
			res.status(401).json("Vous devez vous connecter");
		}
		next();
	} catch (err) {
		res.status(500).json(err.toString());
	}
};
