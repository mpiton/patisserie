import dotenv from "dotenv";
import { NextFunction, Request, Response } from "express";
import jwt from "jsonwebtoken";

dotenv.config();

const secret = process.env.JWT_SECRET;

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export const checkRole = (role: string[] = []) => {
	if (!secret) {
		throw new Error("JWT_SECRET not defined");
	}
	// roles param can be a single role string (e.g. Role.User or 'User')
	// or an array of roles (e.g. [Role.Admin, Role.User] or ['Admin', 'User'])
	if (typeof role === "string") {
		role = [role];
	}

	return [
		// authenticate JWT token and attach user to request object (req.user)

		// authorize based on user role
		(req: Request, res: Response, next: NextFunction) => {
			let cookie: string = req.headers.cookie as string;
			if (cookie) {
				cookie = cookie.replace("t=", "");
			}
			if (!cookie || cookie === undefined) {
				return res.status(401).json({ message: "Vous devez vous connecter" });
			}

			const result: { _id: string; role: string[] } = <
				{ _id: string; role: string[] }
			>jwt.verify(cookie, secret);
			if (role.length && !result.role.includes("ADMIN" || "USER")) {
				// user's role is not authorized
				return res.status(401).json({ message: "Unauthorized" });
			}
			// authentication and authorization successful
			next();
		},
	];
};
